export const addToFavouriteBeers = (beer) => (dispatch) => {
    dispatch({ type: 'ADD_TO_FAVOURITE_BEERS', payload: beer })
};

export const removeFromFavouriteBeers = (beerId) => ({
    type: 'REMOVE_FROM_FAVOURITE_BEERS',
    payload: beerId,
})