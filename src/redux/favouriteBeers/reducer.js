const initialState = {
    status: null,
    data: [],
    errors: null,
}

const favouriteBeers = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TO_FAVOURITE_BEERS':
            return {
                ...state,
                data: [
                    ...state.data,
                    action.payload,
                ]
            };
        case 'REMOVE_FROM_FAVOURITE_BEERS':
            return {
                ...state,
                data: state.data.filter(beer => beer.id !== action.payload)
            };
        default:
            return state;
    }
};

export default favouriteBeers;