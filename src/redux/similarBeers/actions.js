import axios from 'axios';

const API = 'https://api.punkapi.com/v2/';
const PER_PAGE = 4;
const PAGE = 1
const IBU_RANGE = 15;

export const fetchSimilarBeers = (ibu, id) => (dispatch) => {
    dispatch({type: 'FETCH_SIMILAR_BEERS_PENDING'});
    const minIbu = ibu - IBU_RANGE > 0 ? ibu - IBU_RANGE : 1;
    const maxIbu = ibu + IBU_RANGE;
    return axios({
        method: 'get',
        url: `${API}beers?page=${PAGE}&per_page=${PER_PAGE}&ibu_gt=${minIbu}&ibu_lt=${maxIbu}`,
    })
        .then((res) => {
            let filteredArray = res.data.filter(item => item.id !== id)
            if (filteredArray.length > 3) {
                filteredArray.pop();
            }
            dispatch({type: 'FETCH_SIMILAR_BEERS_FULFILLED', payload: filteredArray });
        })
        .catch((error) => {
            dispatch({type: 'FETCH_SIMILAR_BEERS_REJECTED', payload: error});
        });
};