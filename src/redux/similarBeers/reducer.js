import requestStatus from '../../enums/requestStatus';

const initialState = {
    status: null,
    data: [],
    errors: null,
}

const similarBeers = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_SIMILAR_BEERS_PENDING':
            return {
                ...state,
                status: requestStatus.PENDING,
                errors: null,
            };
        case 'FETCH_SIMILAR_BEERS_FULFILLED':
            return {
                ...state,
                status: requestStatus.SUCCESS,
                data: action.payload,
            };
        case 'FETCH_SIMILAR_BEERS_REJECTED':
            return {
                ...state,
                status: requestStatus.ERROR,
                errors: action.payload,
            };
        default:
            return state;
    }
};

export default similarBeers;