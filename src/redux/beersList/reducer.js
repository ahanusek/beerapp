import get from 'lodash/get';
import requestStatus from '../../enums/requestStatus';

const initialState = {
    status: null,
    data: [],
    errors: null,
    page: null,
    fetchingMore: false,
    fetchedAll: false,
}

const beersList = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_BEERS_PENDING':
        return {
            ...state,
            status: requestStatus.PENDING,
            errors: null,
        };
        case 'FETCH_BEERS_FULFILLED':
            return {
                ...state,
                status: requestStatus.SUCCESS,
                data: get(action, 'payload.data', []),
                page: get(action , 'payload.page', 1),
            };
        case 'FETCH_BEERS_REJECTED':
            return {
                ...state,
                status: requestStatus.ERROR,
                errors: action.payload,
            };
        case 'FETCH_MORE_BEERS_PENDING':
            return {
                ...state,
                fetchingMore: true,
                errors: null,
            };
        case 'FETCH_MORE_BEERS_FULFILLED':
            return {
                ...state,
                fetchingMore: false,
                data: [
                    ...state.data,
                    ...get(action, 'payload.data', []),
                ],
                page: get(action , 'payload.page', 1),
            };
        case 'FETCH_MORE_BEERS_REJECTED':
            return {
                ...state,
                status: requestStatus.ERROR,
                errors: action.payload,
                fetchingMore: false,
            };
        case 'NO_MORE_TO_FETCH':
            return {
                ...state,
                fetchingMore: false,
                fetchedAll: true,
            };
        default:
            return state;
    }
};

export default beersList;