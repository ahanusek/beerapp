import axios from 'axios';
import isEmpty from 'lodash/isEmpty';

const API = 'https://api.punkapi.com/v2/';
const PER_PAGE = 10;

export const fetchBeers = (page = 1) => (dispatch) => {
    dispatch({type: 'FETCH_BEERS_PENDING'});
    return axios({
        method: 'get',
        url: `${API}beers?page=${page}&per_page=${PER_PAGE}`,
    })
        .then((res) => {
            const payload = {
                data: res.data,
                page,
            }
            dispatch({type: 'FETCH_BEERS_FULFILLED', payload });
        })
        .catch((error) => {
            dispatch({type: 'FETCH_BEERS_REJECTED', payload: error});
        });
};

export const fetchMoreBeers = () => (dispatch, getState) => {
    const { page } = getState().beers;
    dispatch({type: 'FETCH_MORE_BEERS_PENDING'});
    return axios({
        method: 'get',
        url: `${API}beers?page=${page + 1}&per_page=${PER_PAGE}`,
    })
        .then((res) => {
            const noMoreToLoaded = isEmpty(res.data);
            if (noMoreToLoaded) {
                return dispatch({type: 'NO_MORE_TO_FETCH' });
            }
            const payload = {
                data: res.data,
                page: page + 1,
            }
            dispatch({type: 'FETCH_MORE_BEERS_FULFILLED', payload });
        })
        .catch((error) => {
            dispatch({type: 'FETCH_MORE_BEERS_REJECTED', payload: error});
        });
};