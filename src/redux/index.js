import { combineReducers, applyMiddleware, compose, createStore } from 'redux';
// import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import beersList from './beersList/reducer';
import similarBeers from './similarBeers/reducer';
import favourites from './favouriteBeers/reducer';
// Combine all
const appReducer = combineReducers({
    beers: beersList,
    similarBeers,
    favourites,
});


let middleware = [
    thunk,
];

const store = compose(applyMiddleware(...middleware))(createStore)(appReducer);

export default store;