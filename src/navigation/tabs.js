import React from 'react';
import { Scene, Tabs } from 'react-native-router-flux';
import { Platform, StatusBar } from 'react-native'
import TabIcon from '../components/TabIcon';
import { BeersList, FavouriteBeers } from '../containers'



/* Routes ==================================================================== */
const scenes = (
    <Tabs key={'tabBar'} tabBarPosition="bottom" showLabel={false} pressOpacity={0.95}>
        <Scene
            key="beersList"
            component={BeersList}
            title="Find Your perfect beer!"
            icon={props => TabIcon({ ...props, icon: 'beer', type: "material-community" })}
            navigationBarStyle={{ marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight }}
        />
        <Scene
            key="favouriteBeers"
            component={FavouriteBeers}
            title="Favourite beers"
            icon={props => TabIcon({ ...props, icon: 'favorite' })}
            navigationBarStyle={{ marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight }}
        />
    </Tabs>
);

export default scenes;