import React from 'react';
import { Actions, Stack, Scene } from 'react-native-router-flux';
import { Platform, StatusBar } from 'react-native'
import { BeerDetails } from '../components';
import TabScenes from './tabs';


export default Actions.create(
    <Stack key="root">
        {TabScenes}
        <Scene
            key="beerDetails"
            component={BeerDetails}
            title="Beer details"
            navigationBarStyle={{ marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight }}
        />
    </Stack>);