const app = {
    background: 'inherit',
    cardBackground: '#FFFFFF',
    listItemBackground: '#FFFFFF',
};

const brand = {
    brand: {
        primary: '#212529',
        secondary: '#f5d43d',
        third: '#3B3561',
    },
};

const text = {
    textPrimary: '#222222',
    textSecondary: '#777777',
    success: '#8bc34a',
    alert: '#ED1C24',
    warning: '#faad14',
    headingPrimary: brand.brand.primary,
    headingSecondary: brand.brand.primary,
};

const borders = {
    border: '#D0D1D5',
};


export default {
    ...app,
    ...brand,
    ...text,
    ...borders,
};