import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator, FlatList } from 'react-native';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import { BeerCard, Alert } from '../../components'
import { fetchBeers, fetchMoreBeers } from '../../redux/beersList/actions';
import requestStatus from '../../enums/requestStatus';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    list: {
        justifyContent: 'center',
    },
    loaderContainer: {
        flex: 1,
        justifyContent: 'center',
        marginTop: '30%',
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});


export class BeersList extends Component {
    componentDidMount() {
        this.props.fetchBeers();
    }
    renderContent = () => {
        const { beersStore } = this.props;
        const shouldFetchNextPage = (!isEmpty(beersStore.data) && !beersStore.fetchingMore && !beersStore.fetchedAll);
        if (beersStore.status === requestStatus.PENDING) {
            return (
                <View style={[styles.loaderContainer, styles.horizontal]} >
                    <ActivityIndicator
                        size="large"
                        color="#0000ff"
                    />
                </View>)
        }
        if (beersStore.status === requestStatus.ERROR) {
            return (
                <View style={{ minHeight: 120}} >
                    <Alert
                        message="Error occurred when fetching beers list. You should refresh list and try again fetch data."
                    />
                </View>)
        }
        return (
            <FlatList
                keyExtractor={item => item.id}
                data={beersStore.data}
                refreshing={beersStore.status === requestStatus.PENDING}
                renderItem={({item}) => <BeerCard beer={item} />}
                onEndReached={shouldFetchNextPage && this.props.fetchMoreBeers}
                onRefresh={this.props.fetchBeers}
            />)
    }
    render() {
        return (
            <View
                style={styles.container}
            >
                <View style={styles.list}>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        beersStore: state.beers,
    };
}

export default connect(mapStateToProps, { fetchBeers, fetchMoreBeers })(BeersList);