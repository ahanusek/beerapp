import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import isEmpty from 'lodash/isEmpty';
import { connect } from 'react-redux';
import { BeerCard, Warning } from '../../components'
import { fetchBeers, fetchMoreBeers } from '../../redux/beersList/actions';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    list: {
        justifyContent: 'center',
    },
    loaderContainer: {
        flex: 1,
        justifyContent: 'center',
        marginTop: '30%',
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});


class FavouriteBeers extends Component {
    renderContent = () => {
        const { favouriteBeers } = this.props;
        if (isEmpty(favouriteBeers)) {
            return <View style={{ minHeight: 110 }}>
                <Warning message="List of your favorite beers is empty." />
            </View>
        }
        return (
            <FlatList
                keyExtractor={item => item.id}
                data={favouriteBeers}
                renderItem={({item}) => <BeerCard beer={item} isFavourite />}
            />)
    }
    render() {
        return (
            <View
                style={styles.container}
            >
                <View style={styles.list}>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        favouriteBeers: state.favourites.data,
    };
}

export default connect(mapStateToProps, { fetchBeers, fetchMoreBeers })(FavouriteBeers);