import React from 'react';
import PropTypes from 'prop-types';
import Colors from '../../styles/colors';
import { Icon } from 'react-native-elements';


const TabIcon = ({ icon, focused, type }) => (
    <Icon
        name={icon}
        size={26}
        type={type}
        color={focused ? Colors.brand.secondary : Colors.textSecondary}
    />
);

TabIcon.propTypes = {
    icon: PropTypes.string.isRequired,
    focused: PropTypes.bool.isRequired,
};

export default TabIcon;