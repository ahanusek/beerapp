import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements';
import colors from '../../styles/colors';
import sizes from '../../styles/sizes';
import base from '../../styles/base';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        margin: 20,
        backgroundColor: colors.cardBackground,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: sizes.borderRadius,
    },
    title: {
        color: colors.brand.secondary,
    },
    image: {
        height: 150,
        width: 150,
        marginTop: 10,
        marginBottom: 10,
    },
});

const BeerCard = ({ beer, isFavourite = false }) => (
    <View style={styles.container}>
        <Image
            resizeMode="contain"
            style={styles.image}
            source={{ uri: beer.image_url }}
        />
        <Text style={[base.h4, styles.title]}>
            {beer.name}
        </Text>
        <Text>
            {beer.tagline}
        </Text>
        <Button
            title="See Details"
            icon={{name: 'arrow-right', size: 15, type: 'font-awesome', color: colors.textSecondary }}
            textStyle={{
                fontSize: 14,
                fontWeight: 'bold',
                color: colors.textSecondary,
            }}
            buttonStyle={{
                backgroundColor: 'transparent',
                width: 150,
                height: 40,
                marginTop: 10,
                borderColor: colors.brand.secondary,
                borderWidth: 1,
                borderRadius: sizes.borderRadius,
            }}
            onPress={() => Actions.beerDetails({ beer, isFavourite })}
        />
    </View>
)

export default BeerCard;