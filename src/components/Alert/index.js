import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import { Base, Colors, Sizes } from '../../styles';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: Sizes.padding,
        margin: Sizes.margin,
        backgroundColor: Colors.cardBackground,
        borderRadius: Sizes.borderRadius,
        flexDirection: 'row',
    },
    errorMessage: {
        color: Colors.alert,
        fontSize: 14,
        fontWeight: "700",
        marginLeft: 20,
        flex: 2,
    },
});

const Alert = ({ message }) => (
    <View style={[Base.container, Base.containerCentered, styles.container]}>
        <Icon
            name="alert-circle"
            size={28}
            type="feather"
            color={Colors.alert}
        />
        <Text style={styles.errorMessage}>
            {message}
        </Text>
    </View>
)

export default Alert;