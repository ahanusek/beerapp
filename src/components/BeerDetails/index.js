import React, { Component } from 'react';
import { Text, ScrollView, View, StyleSheet, Image, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { fetchSimilarBeers } from '../../redux/similarBeers/actions';
import { addToFavouriteBeers, removeFromFavouriteBeers } from '../../redux/favouriteBeers/actions';
import { Base, Colors, Sizes } from '../../styles'
import requestStatus from '../../enums/requestStatus';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: Sizes.paddingSml,
        margin: Sizes.padding,
        backgroundColor: Colors.cardBackground,
        borderRadius: Sizes.borderRadius,
    },
    headerContainer: {
        flexDirection: 'row',
    },
    header: {
        width: 0,
        flexGrow: 1,
    },
    title: {
        color: Colors.brand.secondary,
        marginTop: 10,
    },
    image: {
        height: 150,
        width: 100,
        marginTop: 10,
        marginBottom: 10,
    },
    tagline: {
        paddingRight: 5,
    },
    rate: {
        marginBottom: 5,
    },
    greyText: {
        color: Colors.textSecondary,
        fontWeight: '500',
    },
    textContainer: {
        padding: Sizes.padding,
    },
    bestOptions: {
        marginTop: 5,
    },
    sliderItem: {
        margin: 10,
        padding: 10,
        paddingLeft: 20,
        paddingRight: 20,
        borderColor: Colors.brand.secondary,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    sliderTextContainer: {
        width: 100,
        minHeight: 50,
        flex: 1,
        justifyContent: 'center',
    },
    sliderText: {
        fontWeight: '500',
        color: Colors.textSecondary,
        textAlign: 'center',
    }
});

class BeerDetails extends Component {
    componentDidMount() {
        const { beer } = this.props;
        this.props.fetchSimilarBeers(beer.ibu, beer.id)
    }
    handleClickButton = (wasAddedToFavourite) => {
        const { beer, isFavourite } = this.props;
        if (isFavourite) {
            this.props.removeFromFavouriteBeers(beer.id)
            return Actions.favouriteBeers();
        }
        if (wasAddedToFavourite) return this.props.removeFromFavouriteBeers(beer.id);
        this.props.addToFavouriteBeers(beer);
    }
    render() {
        const { beer, similarBeersStore, isFavourite, favouriteBeers } = this.props;
        const wasAddedToFavourite = !!favouriteBeers.find(favBeer => favBeer.id === beer.id)
        return (
            (
                <ScrollView style={[Base.container, styles.container ]}>
                    <View style={styles.headerContainer}>
                        <Image
                            resizeMode="contain"
                            style={styles.image}
                            source={{ uri: beer.image_url }}
                        />
                        <View style={styles.header}>
                            <Text style={[Base.h3, styles.title]}>
                                {beer.name}
                            </Text>
                            <Text style={[Base.h5, styles.tagline]}>
                                {beer.tagline}
                            </Text>
                            <View>
                                <Text style={styles.rate}>
                                    IBU: <Text style={styles.greyText}>{beer.ibu}</Text>
                                </Text>
                                <Text style={styles.rate}>
                                    ABV: <Text style={styles.greyText}>{beer.abv}%</Text>
                                </Text>
                                <Text style={styles.rate}>
                                    EBC: <Text style={styles.greyText}>{beer.ebc}</Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View>
                        <Button
                            title={isFavourite || wasAddedToFavourite ? "Remove from favourites" : "Add to favourites"}
                            icon={{name: isFavourite || wasAddedToFavourite ? 'clear' : 'favorite', size: 15, color: Colors.alert }}
                            textStyle={{
                                fontSize: 14,
                                fontWeight: 'bold',
                                color: Colors.textSecondary,
                            }}
                            buttonStyle={{
                                backgroundColor: 'transparent',
                                width: '100%',
                                height: 40,
                                marginTop: 10,
                                borderColor: Colors.brand.secondary,
                                borderWidth: 1,
                                borderRadius: Sizes.borderRadius,
                            }}
                            onPress={() => this.handleClickButton(wasAddedToFavourite)}
                        />
                    </View>
                    <View style={styles.textContainer}>
                        <Text style={Base.baseText}>
                            {beer.description}
                        </Text>
                        <Text style={Base.baseText}>
                            {beer.brewers_tips}
                        </Text>
                        <Text style={[Base.h5, styles.bestOptions]}>Best served with:</Text>
                        {beer.food_pairing.map(meal => (
                            <Text key={meal}>- {meal}</Text>
                        ))}
                    </View>
                    <View style={styles.textContainer}>
                        <Text>
                            You might also like:
                        </Text>
                        {similarBeersStore.status === requestStatus.PENDING ?
                            <View style={{ margin: 20}}>
                                <ActivityIndicator />
                            </View>
                            :
                            <ScrollView
                                horizontal
                                // pagingEnabled
                                showsHorizontalScrollIndicator={false}
                            >
                                {similarBeersStore.data.map(beer => (
                                    <View style={styles.sliderItem} key={beer.id}>
                                        <Image
                                            resizeMode="contain"
                                            style={styles.image}
                                            source={{ uri: beer.image_url }}
                                        />
                                        <View style={styles.sliderTextContainer}>
                                            <Text style={styles.sliderText}>{beer.name}</Text>
                                        </View>
                                    </View>
                                ))}
                            </ScrollView>
                        }
                    </View>
                </ScrollView>
            )
        )
    }
}

function mapStateToProps(state) {
    return {
        similarBeersStore: state.similarBeers,
        favouriteBeers: state.favourites.data,
    };
}

export default connect(mapStateToProps,
    { fetchSimilarBeers, addToFavouriteBeers, removeFromFavouriteBeers })(BeerDetails);