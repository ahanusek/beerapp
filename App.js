import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-native-router-flux';
import store from './src/redux';
import AppRoutes from './src/navigation';

const App = () => (
    <Provider store={store}>
        <Router scenes={AppRoutes} />
    </Provider>
);


export default App;
